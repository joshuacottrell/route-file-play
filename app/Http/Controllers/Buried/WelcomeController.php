<?php

namespace App\Http\Controllers\Buried;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return view('welcome')->withLatestReload(
            \Carbon\CarbonImmutable::now('EST')->format('H:i:s a')
        );
    }
}
